<?php declare(strict_types=1);

/*
 * This file is part of the REPLACE__projectKeyShort__END package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\REPLACE__phpNamespaceKey__END;

/**
 * Some tools
 *
 * @copyright REPLACE__year__END Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
final class Utils
{

    /**
     * Do something
     *
     * @return string
     */
    public function doSomething(): string
    {
        return '';
    }
}

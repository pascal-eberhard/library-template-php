# REPLACE__projectTitle__END

REPLACE__todo__END

## Description

REPLACE__todo__END

### [Change log](resources/docs/changelog.md)

### [External content](resources/docs/externalContent.md)

### License

The MIT License, see [License File](LICENSE.md).

## Installation

Via composer:

```bash
composer require REPLACE__composerProjectKey__END
```

## Some shell commands

```bash
# All checks
composer checks

# One at a time, see composer.json "scripts"
#composer [insert composer script label, without @ prefix]
```

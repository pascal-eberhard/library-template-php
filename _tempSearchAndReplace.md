# Search and replace

**HINT !:** Delete this file at the end.

## Data

```yaml
_meta:
  search:
    caseSensitive: true
composerProjectKey:
  replace: "pascal-eberhard/REPLACE__todo__END"
  replaceNote: "See composer.json name"
  search: "REPLACE__composerProjectKey__END"
phpNamespaceKey:
  replace: "REPLACE__todo__END"
  replaceNote: "E.g. see composer.json autoload"
  search: "REPLACE__phpNamespaceKey__END"
projectKeyShort:
  replace: "REPLACE__todo__END"
  replaceNote: "See composer.json name, the short part after /"
  search: "REPLACE__projectKeyShort__END"
projectTitle:
  replace: "REPLACE__todo__END"
  replaceNote: "E.g. for the README.md"
  search: "REPLACE__projectTitle__END"
todo:
  replace: "REPLACE__todo__END"
  replaceNote: ""
  search: "REPLACE__todo__END"
year:
  replace: "REPLACE__todo__END"
  replaceNote: "The current year, 4 digits"
  search: "REPLACE__year__END"
```

**HINT !:** Delete this file at the end.
